# !/usr/bin/env python
import sys
import os
import subprocess
import re

class Parser:

    def __init__(self, dev):
        self.dev = dev

    def parse(self):
        process1 = subprocess.Popen(["udevadm", "info", "--query=all", "--name=" + self.dev], stdout=subprocess.PIPE)
        process2 = subprocess.Popen(["egrep", "ID_MODEL=|ID_SERIAL_SHORT="], stdin=process1.stdout, stdout=subprocess.PIPE)
        process1.stdout.close()           
        output = process2.communicate()[0] 
        process2.stdout.close()
        outpuList = output.splitlines()
        self.model = 'n/a'
        self.serial = 'n/a'
        for dataObj in outpuList: 
            if "E: ID_SERIAL_SHORT=" in dataObj:
                regexp = re.search('=(.*)$', dataObj)
                self.serial = regexp.group(1)
            if "E: ID_MODEL=" in dataObj:
                regexp = re.search('=(.*)$', dataObj)
                self.model = regexp.group(1)   

    def getSerial(self):       
        return self.serial
    
    def getModel(self):
        return self.model
        
if __name__ == "__main__":
    if len(sys.argv) != 2:
        sys.exit()

    if not os.path.exists(sys.argv[1]):
        sys.exit()
    
    print sys.argv[1]
    myParser = Parser(sys.argv[1])
    myParser.parse()
    print 'model: ' + myParser.getSerial()
    print 'serial no: ' + myParser.getModel()
    
   